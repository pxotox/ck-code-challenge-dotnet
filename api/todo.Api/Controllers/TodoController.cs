using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TodoApi.Models;

namespace TodoApi.Controllers
{

    // Class to wrap the task list response
    public class ResponseWrap
    {
        public ICollection<TaskItem> result { get; set; }
    }

    [Route("api/v1/tasks")]
    [ApiController]
    public class TodoController : ControllerBase
    {
        private readonly TodoContext _context;

        public TodoController(TodoContext context)
        {
            _context = context;
        }

        // GET: api/v1/tasks
        // Retrieve all tasks
        [HttpGet]
        public ActionResult<ResponseWrap> GetTaskItems()
        {
            ResponseWrap responseWrap = new ResponseWrap();
            responseWrap.result = _context.TaskItems.ToList();
            return Ok(responseWrap);
        }

        // GET: api/v1/tasks/{id}
        // Retrieve an existing task
        [HttpGet("{id}")]
        public ActionResult<TaskItem> GetTaskItem(long id)
        {
            var taskItem = _context.TaskItems.Find(id);

            if (taskItem == null)
            {
                return NotFound();
            }

            return Ok(taskItem);
        }

        // POST: api/v1/tasks
        // Creates a new task
        [HttpPost]
        public ActionResult<TaskItem> PostTaskItem(TaskItem taskItem)
        {
            _context.TaskItems.Add(taskItem);
            _context.SaveChanges();

            return CreatedAtAction("GetTaskItem", new { id = taskItem.Id }, taskItem);
        }

        // PUT: api/v1/tasks/{id}
        // Updates an existing task
        [HttpPut("{id}")]
        public ActionResult<TaskItem> PutTaskItem(long id, TaskItem updateTaskItem)
        {
            var taskItem = _context.TaskItems.Find(id);
            if (taskItem == null)
            {
                return NotFound();
            }

            taskItem.Title = updateTaskItem.Title;
            taskItem.Completed = updateTaskItem.Completed;
            _context.TaskItems.Update(taskItem);
            _context.SaveChanges();

            return Ok(taskItem);
        }
    }
}