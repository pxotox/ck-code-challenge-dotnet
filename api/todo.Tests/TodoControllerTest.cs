using System;
using Xunit;
using Microsoft.AspNetCore.Mvc;
using TodoApi.Controllers;
using TodoApi.Models;

namespace todo.Tests
{
    public class TodoControllerTest : IDisposable
    {
        TodoContextFake _context;
        TodoController _controller;
    
        public TodoControllerTest()
        {
            _context = new TodoContextFake();
            _controller = new TodoController(_context);
        }

        public void Dispose()
        {
            foreach (var entity in _context.TaskItems)
            {
                _context.TaskItems.Remove(entity);
            }                
            _context.SaveChanges();
        }

        private TaskItem _createTask()
        {
            return new TaskItem() {
                Title = "Test #" + new Random().Next(9999),
                Completed = new Random().Next(1) == 1
            };
        }

        private TaskItem _addTask()
        {
            var task = _createTask();
            _context.TaskItems.Add(task);
            _context.SaveChanges();
            return task;
        }
    
        [Fact]
        public void GetTaskItems_WhenCalled_ReturnsOkResult()
        {
            // Act
            var okResult = _controller.GetTaskItems();
    
            // Assert
            Assert.IsType<OkObjectResult>(okResult.Result);
        }

        [Fact]
        public void GetTaskItems_WhenCalled_ReturnsAllItems()
        {
            var taskCount = 3;
            for (int i = 0; i < taskCount; i++)
                _addTask();

            // Act
            var okResult = _controller.GetTaskItems().Result as OkObjectResult;

            // Assert
            var items = Assert.IsType<ResponseWrap>(okResult.Value);
            Assert.Equal(taskCount, items.result.Count);
        }

        [Fact]
        public void GetTaskItem_WhenCalled_ReturnNotFoundResult()
        {
            // Act
            var notFoundResult = _controller.GetTaskItem(new Random().Next(100));
    
            // Assert
            Assert.IsType<NotFoundResult>(notFoundResult.Result);
        }

        [Fact]
        public void GetTaskItem_WhenCalled_ReturnsOkResult()
        {
            var task = _addTask();

            // Act
            var okResult = _controller.GetTaskItem(task.Id);
    
            // Assert
            Assert.IsType<OkObjectResult>(okResult.Result);
        }

        [Fact]
        public void GetTaskItem_WhenCalled_ReturnsItem()
        {
            var task = _addTask();

            // Act
            var okResult = _controller.GetTaskItem(task.Id).Result as OkObjectResult;

            // Assert
            var item = Assert.IsType<TaskItem>(okResult.Value);
            Assert.Equal(item.Id, task.Id);
            Assert.Equal(item.Title, task.Title);
            Assert.Equal(item.Completed, task.Completed);
        }

        [Fact]
        public void PostTaskItem_WhenCalled_ReturnsCreatedAtActionResult()
        {
            var task = _createTask();

            // Act
            var createdResult = _controller.PostTaskItem(task);
    
            // Assert
            Assert.IsType<CreatedAtActionResult>(createdResult.Result);
        }

        [Fact]
        public void PostTaskItem_WhenCalled_ReturnsItem()
        {
            var task = _createTask();

            // Act
            var okResult = _controller.PostTaskItem(task).Result as CreatedAtActionResult;

            // Assert
            var item = Assert.IsType<TaskItem>(okResult.Value);
            Assert.Equal(item.Title, task.Title);
            Assert.Equal(item.Completed, task.Completed);
        }


        [Fact]
        public void PutTaskItem_WhenCalled_ReturnNotFoundResult()
        {
            var task = _createTask();

            // Act
            var notFoundResult = _controller.PutTaskItem(new Random().Next(100), task);
    
            // Assert
            Assert.IsType<NotFoundResult>(notFoundResult.Result);
        }

        [Fact]
        public void PutTaskItem_WhenCalled_ReturnsOkResult()
        {
            var task = _addTask();

            // Act
            var okResult = _controller.PutTaskItem(task.Id, task);
    
            // Assert
            Assert.IsType<OkObjectResult>(okResult.Result);
        }

        [Fact]
        public void PutTaskItem_WhenCalled_UpdatesAndReturnsItem()
        {
            var task = _addTask();
            var updateTask = _createTask();

            // Act
            var okResult = _controller.PutTaskItem(task.Id, updateTask).Result as OkObjectResult;

            // Assert
            var item = Assert.IsType<TaskItem>(okResult.Value);
            Assert.Equal(item.Title, updateTask.Title);
            Assert.Equal(item.Completed, updateTask.Completed);
        }
    }
}
