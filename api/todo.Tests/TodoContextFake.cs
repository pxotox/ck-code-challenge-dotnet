using Microsoft.EntityFrameworkCore;
using TodoApi.Models;

namespace todo.Tests
{
    public class TodoContextFake : TodoContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseInMemoryDatabase(databaseName: "database_name");
        }
    }
}