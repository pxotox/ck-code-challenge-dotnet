# API (Back-end)
This part of the project implements the REST API used by the front-end to retrieve and save data.

# Requirements
To be able to run this part of the project you'll need to have the following requirements met:

* .NET core 2.2

# Quickstart

Here is a quick start to get everything running, you should be able just to copy and paste the following commands and have everything set up.

## Running the application

```
git clone git@bitbucket.org:pxotox/ck-code-challenge-dotnet.git
cd ck-code-challenge-dotnet/api/
dotnet ef database update --project todo.Api
dotnet watch --project todo.Api run
```

## Acessing the application

After running the application you'll be able to access it here: [http://localhost:8000/api/v1/tasks](http://localhost:8000/api/v1/tasks).

## Running tests

To run unit tests you can execute this command: `dotnet test`.

## Updating database schema

If you edit any model (inside `todo.Api/Models`) and need to update the database schema you can execute these commands:

* Create new migration file: `dotnet ef migrations add <Migration-Name> --project todo.Api`
* Update database: `dotnet ef database update`
